# Welcome to Rust-World

### Part 1: [Hello World](part-1-hello-world.md)
### Part 2: [Common concepts](part-2-common-concepts.md)
### Part 3: [Struct & Enum](part-3-struct-and-enum.md)
### Part 4: [Naming convention](part-4-naming-convention.md)
### Part 5: [String and &str](part-5-string-in-rust.md)
### Part 6: [Core concepts](part-6-core-concepts.md)
### Part 7: [Generic & Lifetime](part-7-generic-and-lifetime.md)
### Part 8: [Trait](part-8-trait.md)
### Part 9: [Collections](part-9-collections.md)
