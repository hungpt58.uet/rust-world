# I. Hello World
```rust
// `Main` function always runs first
fn main() {
    println!("Hello world!");
}
```

# II. Run by Rustc

Check rust install on your machine: 
```shell
 rustc --version
```

### Build & Run

Build into binary by this command:
```shell
rustc main.rs
```

On Linux or MacOs, run this command:
```shell
./main
```

On Windows, run this command:
```shell
.\main.exe
```

View the result on your terminal

# III. Run by Cargo

Check cargo install on your machine:
```shell
cargo --version 
```

Create a new project:
```shell
cargo new
```

### Build & Run

Use your terminal and go to project folder

Build into binary by this command (dev mode):
```shell
cargo build
```

Run by this command:
```shell
cargo run
```

View the result on your terminal

### Other Info

Use `cargo check` instead of `cargo build` for faster. Because, `build` produces an executable file ~> slow, `check` only compiles without producing

