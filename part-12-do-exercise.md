# Do exercise in Rust

### Bài 1
YC: Tính S(n) = 1 + 2 + 3 + … + n
```rust
fn sum(n: u32) -> u32 {
    let mut total: u32 = 0;
    
    for i in 1..=n {
        total += i;
    }
    
    return total;
}
```

### Bài 2
YC: Đếm số các số nguyên tố từ 1 ~> n
```rust
fn is_snt(n: u32) -> bool {
    if n == 1 || n == 2 { return true }

    let half = (n as f64).sqrt() as u32;
    for i in 2..=half {
        if n % i == 0 { return false }
    }

    return true
}

fn count_snt(n: u32) -> u32 {
    let mut total: u32 = 0;

    for i in 1..=n {
        if is_snt(i) { total += 1; }
    }

    return total
}
```

### Bài 3
YC: Tính tổng các số trong mảng
```rust
fn get_total(arr: Vec<u8>) -> u8 {
    let mut total = 0;

    for i in 0..arr.len() {
        total += &arr[i];
    }

    total
}

fn get_total_use_fold(arr: Vec<u8>) -> u8 {
    arr.iter().fold(0, |x, y| x + y)
}
```

### Bài 4
YC: Hãy sử dụng vòng lặp for để xuất tất cả các ký tự từ A đến Z
```rust
fn show_character() {
    for c in 'A'..='Z' {
        println!("{}", c);
    }
}
```

### Bài 5
YC: Tìm số lớn nhất và nhỏ nhất trong mảng
```rust
fn find_max_min(arr: [u8; 5]) {
    let mut max = &arr[0];
    let mut min = &arr[0];

    for i in 1..arr.len() {
        max = max.max(&arr[i]);
        min = min.min(&arr[i]);
    }

    println!("Max = {} and Min = {}", max, min)
}
```

### Bài 6
YC: Cho chuỗi: Xin chào Việt Nam thân mến ~~> XinChàoViệtNamThânMến
```rust
fn format_str(str: &str) -> String {
    let mut output: String = String::new();

    for word in str.split_whitespace() {
        let mut ch: Vec<char> = word.chars().collect();
        ch[0] = ch[0].to_uppercase().nth(0).unwrap();

        let new_word = String::from_iter(ch);
        output.push_str(&new_word);
    }

    output
}
```

### Bài 7
YC: Tính tổng 2 số sử dụng tham chiếu
```rust
fn add(x1: &u8, x2: &u8) -> u8 {
    x1 + x2
}
```