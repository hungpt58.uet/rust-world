# I. VARIABLE & CONST & SCOPE

There are 2 type of variable in rust-lang: mutable and immutable

### Immutable variable
- Don't change value
- Takes advantage of the safety and easy concurrency
```rust
let name: &str   = "Messi";
let name: String = String::from("Hello Messi");

let block_val = {
    println!("Variable with block code");
    age + 1
};
```

### Mutable variable
- Can change value
- By adding `mut` in front of the variable name
```rust
let mut age = 1;
age = 2;
```

### Const variable
Rust has two different types of constants which can be declared in any scope including global:
- const: An unchangeable value (the common case)
- static: A possibly mutable variable with 'static lifetime. Accessing or modifying a mutable static variable is unsafe

There are few differences between constants and variables:
- No allowed to use mut with constants
- Declare constants using the const keyword instead of the let keyword
- The type of the value must be annotated

```rust
const VN_CAPITAL: &str = "Ha Noi";
```

Constants can be declared in any scope, including the global scope, which makes them useful for values that many parts of code need to know about

Constants are valid for the entire time a program runs, within the scope they were declared in

### Shadowing
The first variable is shadowed by the second, which means that the second variable is what the compiler will see when you use the name of the variable
```rust
let shadow_val = 1; // this shadow_val is shadow
let shadow_val = 2; 
```

### Scope
Variable bindings have a scope, and are constrained to live in a block

A block is a collection of statements enclosed by braces {}
```rust
let x1    = 1; // This binding lives in the main function
let mut y = 1;

{ // This is a block, and has a smaller scope than the main function
    // The bindings x1 & x2 only exist in this block
    let x1 = 2;
    let x2 = 2;

    let y1 = y; // assign mut variable to immut variable. No change value of y if y1 lives => It is called: Freezing
}
```

### RAll
Variables in Rust do more than just hold data in the stack: they also own resources

Rust enforces RAII (Resource Acquisition Is Initialization), so whenever an object goes out of scope, its destructor is called and its owned resources are freed.

```rust
fn create_box() {
    // Allocate an integer on the heap
    let _box1 = Box::new(3i32);
}  // `_box1` is destroyed here, and memory gets freed
```

# II. DATA TYPE
Every value is of a certain data type, which tells what kind of data is being specified to know how to work with that data

Rust is a statically typed language, which means that it must know the types of all variables at compile time

Primitive Data Types in rust include: Scalar type, Compound type

### Scalar type
A scalar type represents a single value

Rust has four primary scalar types: integers, floating-point numbers, booleans and characters

Integer type includes 2 forms: Signed(`i`) & Unsigned(`u`)
```text
i8    - u8    - length: 8 bit
i16   - u16   - length: 16 bit
i32   - u32   - length: 32 bit
i64   - u64   - length: 64 bit
i128  - u128  - length: 128 bit
isize - usize - length: depend on the architecture of the computer: x64 ~> 64 bit and x86/x32 ~> 32 bit
```

Signed formula of length: -2^(n - 1) to 2^(n - 1) - 1 ~> n: bit

Unsigned formula of length: 0 to 2^n - 1
```rust
let n1 = 1000;
let n1: u32 = 1000;
let n1 = 1_000;
let n1 = 0xff;
let n1 = 1u8; // equal: let n1: u8 = 1
```

Float-point types are f32 & f64. Default is f64 because speed is same as f32
```rust
let fl_val = 1.0;
let fl_val: f32 = 1.0;
```

Boolean type have 2 values: true or false. It's one byte in size
```rust
let b_val = true;
let b_val: bool = false;
```

Character type is declared with single quotes. It is four bytes in size and represents a Unicode Scalar Value
```rust
let c = 'z';
let c: char = 'z';
let c = '😻';
```

Type casting can be performed using the `as` keyword
```rust
let decimal = 65.4321_f32;
let idx: u32 = decimal as u32;
```

### Compound type
Can group multiple values into one type

Rust has two primitive compound types: tuple and array

### Tuple
A tuple is a general way of grouping variety of types into one

Tuples have a fixed length: once declared, they cannot grow or shrink in size

The tuple without any values has a special name ~~> unit ~~> symbol: () ~> represent an empty value or an empty return type
```rust
let tuple_val = (1, 2, 3);
let tuple_val: (u8, u8, char) = (1, 2, 'z');
let (v1, v2, v3) = tuple_val; // destructure a tuple value
let first_element = tuple_val.0; // access the first element in tuple
```

### Array
Array is allocated on the stack rather than the heap

Arrays are more useful when you know the number of elements will not need to change
```rust
let arr = [1, 2, 3, 4, 5];
let arr: [u8; 2] = [1, 2];
let first_element = arr[0]; // access element in array
```

### Alias type
```rust
fn main() {
    type X = u8;
    let a: X = 1;
}
```

# III. CONTROL FLOW

### If
```rust
let n = 1;
if n == 1 {
    println!("Value equals to 1")
} else if n > 1 {
    println!("Value is greater than 1")
} else {
    println!("Value is less than 1")
}

// `if` returns value
let r = if n == 1 { 6 } else { 0 };
```

### Loop
Loop can return value
```rust
let mut i: u8 = 0;
let val = loop {
    i += 1;
    println!("Using loop kind: {:?}", i);
    if i >= 3 { break 10; }
};
```

### While
```rust
while i < 5 { i += 1; }
```

### For
```rust
for item in arr { println!("Element in arr: {}", item); }
```

use range with for
```rust
for i in 0..=1 { println!("Element in arr: {}", arr[i]); }
for i in 0..2 { println!("Element in arr: {}", arr[i]); }
```

### Break & Continue
Same to other languages as Java or C/C++
- Break: exit context loop
- Continue: Start new loop
```rust
for i in 0..=1 {
    if i == 0 { continue; }
    println!("Continue sample: {}", i);
}
```

### Loop label
Can set label to `loop`, `while` and `for`

Use loop label with `continue` or `break`

It is useful in case: nested multi loop
```rust
'lab: loop { break 'lab; }
'lab: while 1 == 1 { break 'lab; }
'lab: for item in arr { continue 'lab; }
```

# IV. FUNCTION
Function is created by using key word `fn`
```rust
fn add_total(x1: u8, x2: u8) -> u8 { // return type is u8 by using `->`
    x1 + x2 // or return x1 + x2
} // The last line without `;` will implicitly return
println!("Total 1 + 2 = {:?}", add_total(1, 2));

fn show() { println!("Hello World"); } // same: fn show() -> () { println!("Hello World"); }
show();
```

# V. COMMENT
There are 2 comment types: Regular comments and Doc comments
+ Regular comments: are ignored by the compiler. They're forms: // or /* ... */
+ Doc comments: are parsed into HTML library documentation. They're forms: /// or //!
```rust
let x = 5 + /* 90 + */ 5;
println!("Is `x` 10 or 100? x = {}", x);
```


