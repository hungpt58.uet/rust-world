# I. Struct
Struct holds multiple related values with different types

There are three types of structures ("structs") that can be created using the `struct` keyword:
- Tuple structs, which are, basically, named tuples
- The classic [C structs](https://en.wikipedia.org/wiki/Struct_(C_programming_language))
- Unit structs, which are field-less, are useful for generics

### Classic C struct
We create an instance of that struct by specifying concrete values for each of the fields

We don’t have to specify the fields in the same order in which we declared them in the struct
```rust
struct Person {
    pub name: String,
    pub age: u8
}

fn main() {
    let person = Person {
        age: 1,
        name: String::from("My Name")
    };
    println!("Your name: {}", person.name);
}
```

The fields are mutable or immutable in struct depend on `instance` which is created
```rust
fn main() {
    let person = Person { // This is immutable instance
        age: 1,           // Field is immutable too ~> No change value
        name: String::from("My Name")
    };

    let mut person = Person { // This is mutable instance
        age: 1,               // Field is mutable too ~> Change value
        name: String::from("My Name")
    };
    person.age = 2;
}
```

Field Init Shorthand: The parameter names and the struct field names are exactly the same
```rust
fn new_person(name: String, age: u8) -> Person {
    Person { name, age }
}
```

Copy with edit:
```rust
fn main() {
    let person = Person { age: 1, name: String::from("My Name") };

    let mut person2 = Person { 
        age: 2,   // Change value of `age`            
        ..person // Copy all another value of fields from `person` ( except `age` ). Always last line
    };
}
```

### Tuple Structs
Rust also supports structs that look similar to tuples, called tuple structs
```rust
struct Color(i32, i32, i32);
struct Point(i32, i32, i32);

fn main() {
    let black = Color(0, 0, 0);  // black and origin values are different types
    let origin = Point(0, 0, 0); // even though the fields within the struct might have the same types
}
```

### Unit struct
```rust
struct AlwaysEqual;

fn main() {
    let subject = AlwaysEqual;
}
```

### Struct Implementation
```rust
struct Person {
    pub name: String,
    pub age: u8
}

impl Person {
    // this is a method
    fn add_one(&self) -> u8 { //&self is short-hand for `self: &Self`
        self.age + 1
    }

    // this is a method
    fn update(&mut self) { // update without return
        self.age = self.age + 1;
    }
}

impl Person {
    // this is an associated function
    fn new(age: u8) -> Self {
        Person { age, name: String::from("Hello World") }
    }
}
```
We can have multiple `impl` block

Function in `impl` struct block with `self` keyword is called `method`. It belongs to an instance

If having no `self` keyword, it will be an associated function of struct

Compare to Java: method = normal method and associated function = static method
```rust
fn main() {
    let p = Person { age: 1, name: String::from("hung") };
    let v = p.add_one(); // only call by an instance
    
    let p = Person::new(1); // can call by struct
}
```

# II. Enum
```rust
enum Event {
    DeletePost,
    CreatePost(String),
    User { id: u8, name: &'static str }
}

impl Event {
    fn say(&self) {
        match self {
            Event::DeletePost => println!("DeletePost"),
            Event::CreatePost(str) => println!("CreatePost: {}", str),
            _ => println!("Other")
        }
    }
}

fn main() {
    Event::DeletePost.say();
}
```

### Option
The Option<T> is enum which has two variants:
- None, to indicate failure or lack of value
- Some(value), a tuple struct that wraps a value with type T
```rust
fn main() {
    let opt_v_1: Option<u8> = Some(1);
    let opt_v_2: Option<u8> = None; // same None::<i32>;

    let v: u8 = opt_v_1.unwrap(); // Get value inside Option
    
    match opt_v_1 {
        Some(v) => println!("V = {}", v),
        None    => println!("None value")
    };
}
```
