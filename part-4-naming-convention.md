# Naming convention
Ref: https://rust-lang.github.io/api-guidelines/naming.html
```text
Modules	      -> snake_case
Types	      -> UpperCamelCase
Traits	      -> UpperCamelCase
Enum variants -> UpperCamelCase
Functions     -> snake_case
Methods	      -> snake_case
Macros	      -> snake_case!

Local variables -> snake_case
Statics	        -> SCREAMING_SNAKE_CASE
Constants       -> SCREAMING_SNAKE_CASE
Type parameters -> concise UpperCamelCase, usually single uppercase letter: T
Lifetimes       -> short lowercase, usually a single letter: 'a, 'de, 'src
```

### Ad-hoc conversions
```text
1. as_    ~~> [ borrowed -> borrowed ]                 ~~> Good speed
2. to_    ~~> [ borrowed -> borrowed ]                 ~~> Bad speed
          ~~> [ borrowed -> owned (non-Copy types) ]
          ~~> [ owned -> owned (Copy types) ]
3. into_  ~~> [ owned -> owned ]                       ~~> Unclear     
```

### Some common traits
1. The most important common traits to implement from std are:
- Copy
- Clone
- Eq
- PartialEq
- Ord
- PartialOrd
- Hash
- Debug
- Display
- Default

2. The following conversion traits should be implemented where it makes sense:
- From
- TryFrom
- AsRef
- AsMut

3. The following conversion traits should never be implemented:
- Into
- TryInto

4. Collections
- FromIterator 
- Extend
