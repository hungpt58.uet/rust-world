# String In Rust
There are two types of strings in Rust: `String` and `&str`
- A `String` is stored as a vector of bytes (Vec<u8>).  Always be a valid UTF-8 sequence. String is heap allocated, growable and not null terminated.
  Its size is unknown at compile time. It is mutable. With `String` type, The memory must be requested from the memory allocator at runtime. 
  We need a way of returning this memory to the allocator when we’re done with our String

- `&str` is a slice (&[u8]) that always points to a valid UTF-8 sequence. Its size is known at compile time.
  `&str` can be on the heap, the stack, or static directly from the executable. It is immutable.
   We know the contents at compile time, so the text is hardcoded directly into the final executable. This is why string literals are fast and efficient

```rust
let string_v: String = String::from("hello");
let str: &str        = "hello";
```

### Updating a String
```rust
fn main() {
    let mut s = String::from("foo");
    s.push_str("bar");

    let s1 = String::from("Hello, ");
    let s2 = String::from("world!");
    let s3 = s1 + &s2; // Same: format!("{}{}", s1, s2);
}
```

### Conversion
```rust
let str_v: String = String::from("hihi");
let str: &str     = str_v.as_str();
let str_v: String = str.to_string();
```

If we have a function takes `&str` as a parameter. We can pass `&str` or `&String` to that function. 
Because `&String` can be deref-coerced to `&str` by compiler

### Slicing Strings
```rust
let hello = "Здравствуйте";
let s = &hello[0..4]; // ~> "Здра" ~> only for form: &str and not form: String
```

### Literals and escapes
```rust
// If you need quotes in a raw string, add a pair of #s
let quotes = r#"And then I said: "There is no escape!""#;

// If you need "#" in your string, just use more #s in the delimiter
let longer_delimiter = r###"A string with "# in it. And even "##!"###;
```

### Some samples
```rust
fn main() {
    let pangram: &'static str = "the quick brown fox jumps over the lazy dog";
    // Iterate over words in reverse, no new string is allocated
    for word in pangram.split_whitespace().rev() {
        println!("> {}", word);
    }

    // Copy chars into a vector, sort and remove duplicates
    let mut chars: Vec<char> = pangram.chars().collect();
    chars.sort();
    chars.dedup();

    // Create an empty and growable `String`
    let mut string = String::new();
    for c in chars {
        // Insert a char at the end of string
        string.push(c);
        // Insert a string at the end of string
        string.push_str(", ");
    }

    // The trimmed string is a slice to the original string, hence no new allocation is performed
    let chars_to_trim: &[char] = &[' ', ','];
    let trimmed_str: &str = string.trim_matches(chars_to_trim);

    // Heap allocate a string
    let alice = String::from("I like dogs");
    // Allocate new memory and store the modified string there
    let bob: String = alice.replace("dog", "cat");
}
```

### Should use `String` or `&str`
- Use `String` for infor can be updated and `&str` is for read-only info
- Normally, `String` is used in domain model. `&str` is in record model (as DTO, DB Record ...)

