# I. Ownership
Ownership is Rust’s unique feature. It enables Rust to make memory safety guarantees without needing garbage collector

Ownership is a set of rules that a Rust program manages memory

```text
Some languages have garbage collection(GC) that regularly looks for no-longer used memory. 
In other languages, the programmer must explicitly allocate and free the memory. 
Rust uses a third approach: memory is managed through a system of ownership with a set of rules that the compiler checks. 
If any of the rules are violated, the program won’t compile. None of the features of ownership will slow down your program while it’s running.
```

### Stack & Heap
Both the stack and the heap are parts of memory available to your code to use at runtime, but they are structured in different ways

Stack:
```text
- The stack stores values in the order it gets them and removes the values in the opposite order (LIFO)
- All data stored on the stack must have a known, fixed size
```

Heap:
```text
- Data with an unknown size at compile time or a size that might change must be stored on the heap
- When you put data on the heap, you request a certain amount of space. The memory allocator finds an empty spot in the heap that is big enough, marks it as being in use, and returns a pointer, which is the address of that location.
- This process is called allocating on the heap and is sometimes abbreviated as just allocating
- The pointer to the heap is a known, fixed size, you can store the pointer on the stack
```

Pushing to the stack is faster than allocating on the heap because the allocator never has to search for a place to store new data, that location is always at the top of the stack

Allocating space on the heap requires more work, because the allocator must first find a big enough space to hold the data and then perform bookkeeping to prepare for the next allocation

Accessing data in the heap is slower than accessing data on the stack because you have to follow a pointer to get there

### Ownership rules
- Each value in Rust has an owner.
- There can only be one owner at a time.
- When the owner goes out of scope, the value will be dropped

```rust
fn main() {
    {                      // s is not valid here, it’s not yet declared
        let s = "hello";   // s is valid from this point forward

        // do stuff with s
    }                      // this scope is now over, and s is no longer valid
}
```

### Ways Variables and Data Interact
```rust
fn main() {
    let x = 5;
    let y = x; // This is copy value. Apply for all types that knows size at compile time
    println!("{} - {}", x, y); // x is valid

    let s1 = String::from("hello");
    let s2 = s1; // s1 is invalid because value of s1 is moved into s2

    let s3 = String::from("hello");
    let s4 = s3.clone(); // s3 is valid to use
}
```

Rust has special annotation is called: `Copy` trait that if implement it, type is stored on the stack

`Copy` trait is only for type that is a group of simple scalar values, because scalar values are on the stack.

Passing a variable to a function will move or copy (move for data on heap and copy for data on stack)

# II. References and Borrowing
A reference is guaranteed to point to a valid value of a particular type for the life of that reference

We call the action of creating a reference `borrowing`
```rust
fn calculate_length(s: &String) -> usize { // s is a reference to a String
    s.len()
} // Here, s goes out of scope. But because it does not have ownership of what
  // it refers to, it is not dropped.
```

There are 2 form of borrowing: mutable and immutable
- Mutable is `&`      ~~> Read-only
- Immutable is `&mut` ~~> Read and change value

There is no borrowing as mutable more that once at a time. The benefit of having this restriction is that Rust can prevent data races at compile time 

A data race is similar to a race condition and happens when these three behaviors occur:
- Two or more pointers access the same data at the same time
- At least one of the pointers is being used to write to the data
- There’s no mechanism being used to synchronize access to the data

Rust prevents this problem by refusing to compile code with data races!
```rust
let mut s = String::from("hello");

let r1 = &mut s;
let r2 = &mut s; // ERROR because there are 2 mutable reference
```

```rust
let mut s = String::from("hello");

let r1 = &s;     // no problem
let r2 = &s;     // no problem
let r3 = &mut s; // ERROR because have a mutable reference while having an immutable one to the same value
                 // If r3 changes value and r1, r2 is living as immutable. It is wrong 
println!("{}, {}, and {}", r1, r2, r3);
```

We can fix above code 
```rust
let mut s = String::from("hello");

let r1 = &s; // no problem
let r2 = &s; // no problem
println!("{} and {}", r1, r2);
// variables r1 and r2 will not be used after this point

let r3 = &mut s; // no problem
println!("{}", r3);
```

The ref pattern
```rust
// ref = & ~> borrow reference
let c = 'Q';
let ref ref_c1 = c; // equal ~> let ref_c2 = &c;
```

### Partial moves
Partial move: both by-move and by-reference pattern bindings can be used at the same time
```rust
struct Person {
    name: String, // value
    age: Box<u8>, // reference
}

fn main() {
    let person = Person { // create new instance of Person
        name: String::from("Alice"),
        age: Box::new(20),
    };

    // `name` is moved out of person, but `age` is referenced
    let Person { name, ref age } = person;

    // `person` and `person.name` cannot be used but `person.age` can be used as it is not moved
    // Partial move of `person`
    println!("The person's age from person struct is {}", person.age);
}
```

### Dangling References (Treo tham chiếu)
A point references a location in memory that is freed

The Rust compiler guarantees that references will never be dangling references: if you have a reference to some data, the compiler will ensure that the data will not go out of scope before the reference to the data does.
```rust
// ERROR EXAMPLE
fn dangle() -> &String {
    let s = String::from("hello"); // s only lives in the scope of function
    &s                             // If out of this scope, s is dropped. So, reference is invalid
}

fn wrong(str: String) -> &String { // error by str is moved in function. So, it is only valid in scope of function 
    &str
}

// OK EXAMPLE
fn right(str: &mut String) -> &String { // str is borrowed from out of function. So, it is valid when function is over
    str.push_str("hihi");
    str
}
```

# III. The Slice Type
Slices let you reference a contiguous sequence of elements. A slice is a kind of reference, so it does not have ownership.

```rust
fn main() {
    let s = String::from("hello world");

    let hello = &s[0..5];   // Same &[..5]
    let world = &s[6..11];  // Same &[6..]
    let slice = &s[0..len]; // Same &[..]

    let a = [1, 2, 3, 4, 5];
    let slice = &a[1..3];
}
```
