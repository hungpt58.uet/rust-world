# Generic
Generics is the topic of generalizing types and functionalities to broader cases

This is extremely useful for reducing code duplication in many ways, but can call for rather involved syntax

Using generic types won't make your program run any slower than it would with concrete types.

Rust process of turning generic code into specific code by filling in the concrete types that are used when compiled

### Struct
```rust
struct Point<T> {
    x: T,
    y: T,
}

fn main() {
    let integer = Point { x: 5, y: 10 };
    let float = Point { x: 1.0, y: 4.0 };
}
```

### Enum 
```rust
enum Option<T> {
    Some(T),
    None,
}
```

### Method
```rust
struct Point<T> {
    x: T,
    y: T,
}

impl<T> Point<T> {
    fn x(&self) -> &T {
        &self.x
    }
}

fn printer<T>(t: T) {
    println!("{}", t);
}
```

### Bounds
```rust
// Define a function `printer` that takes a generic type `T` which must implement trait `Display`.
fn printer<T: Display>(t: T) {
    println!("{}", t);
}

struct S<T: Display>(T);

fn compare_prints<T: Debug + Display>(t: &T) {}

fn compare_prints_2<T>(t: &T) where T: Debug + Display {}
```

### Associated types
```rust
// `A` and `B` are defined in the trait via the `type` keyword.
// (Note: `type` in this context is different from `type` when used for aliases).
trait Contains {
    type A;
    type B;

    // Updated syntax to refer to these new types generically.
    fn contains(&self, _: &Self::A, _: &Self::B) -> bool;
}

// Using associated types
fn difference<C: Contains>(container: &C) -> i32 { ... }
```

### Phantom type parameters
A phantom type parameter is one that doesn't show up at runtime, but is checked statically (and only) at compile time

```rust
// At compile time, there are 2 param in struct
struct PhantomTupleCompileTime<A, B>(A, PhantomData<B>);

// At run time, there is only one
struct PhantomTupleRunTime<A>(A);

fn main() {
    let x1: PhantomTupleCompileTime<char, f32> = PhantomTupleCompileTime('c', PhantomData);
    let x2: PhantomTupleCompileTime<char, f64> = PhantomTupleCompileTime('c', PhantomData);
    
    // x1 different x2. 
}
```

### Lifetimes
Lifetimes are another kind of generic, ensure that references are valid as long as we need them to be

The borrow checker uses explicit lifetime annotations to determine how long references should be valid

Lifetime annotations don’t change how long any of the references live

They describe the relationships of the lifetimes of multiple references to each other without affecting them live

```text
foo<'a>      // `foo` has a lifetime parameter `'a`
foo<'a, 'b>  // `foo` has lifetime parameters `'a` and `'b`. The lifetime of foo cannot exceed that of either 'a or 'b.
```

```rust
struct ImportantExcerpt<'a> {
    part: &'a str,
}

impl<'a> ImportantExcerpt<'a> {
    fn level(&self) -> i32 {
        3
    }
}

fn print_refs<'a, 'b>(x: &'a i32, y: &'b i32) {
    println!("x is {} and y is {}", x, y);
}

// Mutable references are possible with lifetimes as well.
fn add_one<'a>(x: &'a mut i32) {
    *x += 1;
}

// Returning references must be within the correct lifetime.
fn pass_x<'a, 'b>(x: &'a i32, _: &'b i32) -> &'a i32 { x }

// ERROR: because 'a is pass from outside ~> lifetime 'a is big than function.
//        but `&String::from("foo")` is dropped ~> its lifetime is shorter than 'a
fn invalid_output<'a>() -> &'a String { &String::from("foo") }

fn main() {
    let (four, nine) = (4, 9);
    // The lifetime of `four` and `nine` must be longer than that of `print_refs`.
    print_refs(&four, &nine);
}
```

Lifetime elision rules(only for function): a set of particular cases that the compiler will consider, and if your code fits these cases, you don’t need to write the lifetimes explicitly
- The first rule is that the compiler assigns a lifetime parameter to each parameter that’s a reference
```text
fn foo(x: &i32){}
        ||
fn foo<'a>(x: &'a i32){} // compiler add lifetime automatically

fn foo(x: &i32, y: &i32);
                ||
fn foo<'a, 'b>(x: &'a i32, y: &'b i32);
```

- The second rule is that if there is exactly one input lifetime parameter, that lifetime is assigned to all output lifetime parameters
```text
fn foo(x: &i32) -> &i32{}
        ||
fn foo<'a>(x: &'a i32) -> &'a i32 // compiler add lifetime automatically
```

- The third rule is that if there are multiple input lifetime parameters, but one of them is `&self` or `&mut self`, the lifetime of self is assigned to all output lifetime parameters. 
```text
fn foo(&self, x: &i32) -> &i32 {}
            ||
fn foo<'a, 'b>(&'a self, x: &'b i32) -> &'a i32{}            
```

The Static Lifetime: that the affected reference can live for the entire duration of the program

There are two ways to make a variable with 'static lifetime, and both are stored in the read-only memory of the binary:
- Make a constant with the static declaration
- Make a string literal which has type: `&'static str`

All string literals have the `'static` lifetime
```rust
let x = "hihi"; // same: let x: &'static str = "hihi";
```

Lifetimes use bounds as well
- `T: 'a`: All references in T must outlive lifetime 'a.
- `T: Trait + 'a`: Type T must implement trait `Trait` and all references in T must outlive 'a
```rust
struct Ref<'a, T: 'a>(&'a T);
```

