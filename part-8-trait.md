# Generic ~~> Trait
A trait is a collection of methods. Traits can be implemented for any data type

```rust
trait Behavior {
    fn say();
    fn show_msg(&self);
    
    fn me() { println!(""); } // default method
}

struct Bird;

impl Behavior for Bird {
    fn say() { println!("This is a bird"); }
    fn show_msg(&self) {
        println!("This is a bird with message");
    }
}

fn main() {
    Bird::say();
    Bird.show_message();
}
```

### Derive
The compiler is capable of providing basic implementations for some traits via the `#[derive]`:
- Comparison traits: `Eq, PartialEq, Ord, PartialOrd`
- `Clone`, to create T from &T via a copy.
- `Copy`, to give a type 'copy semantics' instead of 'move semantics'.
- `Hash`, to compute a hash from &T.
- `Default`, to create an empty instance of a data type.
- `Debug`, to format a value using the {:?} formatter.

View other traits at [### Some common traits](part-4-naming-convention.md#Some common traits)

```rust
impl Default for Bird { // or use derive on struct: #[derive(Default)]
    fn default() -> Self { Bird }
}

fn main() {
    Bird::default();
}
```

### Returning A Trait
The Rust compiler needs to know how much space every function's return type requires. This means all your functions have to return a concrete type

Instead of returning a trait object directly, our functions return a Box which contains some `T`. A box is just a reference to some memory in the heap. Because a reference has a statically-known size, and the compiler can guarantee it points to a heap-allocated `T`

Need to write the return type with the `dyn` keyword 
```rust
fn return_trait() -> Box<dyn Behavior> {
    // Do something here
}

pub fn test(flag: bool) -> Box<dyn Behavior> { //trait Behavior requires all methods have `&self` 
    if flag {
        Box::new(Bird)
    } else {
        Box::new(Fish)
    }
}
```

### Impl Traits
As Parameters
```rust
pub fn notify(item: &impl Behavior) {
    item.show_msg()
}

pub fn notify_2(item: impl Behavior) {
    item.show_msg()
}
```

As return type: useful in the context of closures and iterators. It only uses in function that returns one type.
If having more one type, you will get error
```rust
pub fn notify() -> impl Behavior {
    Bird::default() // This is right
}

// This is wrong because having two impl types are returned
pub fn notify_2(tag: bool) -> impl Behavior {
    if tag {
        Bird::default()
    } else {
        Fish::default()
    }
}
```


### Trait Bound Syntax
```rust
pub fn notify<T: Behavior>(item: &T) {
    item.show_msg();
}

pub fn notify_multi(item: &(impl Behavior + Display)) { // Multi Trait Bound
    // Do something
}

pub fn notify_multi_2<T: Behavior + Display>(item: &T) { // Multi Trait Bound
    // Do something
}

pub fn notify_multi_3<T>(item: &T) -> u32 where T: Behavior + Display { // Multi Trait Bound
    // Do something
}
```

```rust
struct Pair<T> { x: T, y: T }

impl<T: PartialOrd> Pair<T> { // IMPL PartialOrd trait for generic Struct
    fn cmp_display(&self) {
        if self.x >= self.y {
            println!("The largest member is x = {}", self.x);
        } else {
            println!("The largest member is y = {}", self.y);
        }
    }
}
```

### Super traits

```rust
trait Person {
    fn name(&self) -> String;
}

// Person is a super trait of Student.
// Implementing Student requires you to also impl Person.
trait Student: Person {
    fn university(&self) -> String;
}
```

### Multi trait have same method name
```rust
trait UsernameWidget {
    fn get() -> String;
}

trait AgeWidget {
    fn get() -> u8;
}

impl UsernameWidget for Bird {
    // override function here
}

impl AgeWidget for Bird {
    // override function here
}

// Bird has two functions that have same name
fn main() {
    <Bird as UsernameWidget>::get(); // assign specific trait used
}
```
