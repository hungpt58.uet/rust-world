# Vector
Vectors allow you to store more than one value in a single data structure that puts all the values next to each other in memory

Vectors can only store values of the same type

```rust
fn main() {
    let v: Vec<i32> = Vec::new(); // full form
    v.push(5);
    v.push(6);
    v.push(6);

    let v = vec![1, 2, 3];        // macro form
    
    // Reading Elements of Vectors
    let third: &i32 = &v[2];     
    let third: Option<&i32> = v.get(2);
    
    // use `&` to borrow not own
    for i in &v { println!("{}", i); }
}
```

```rust
fn main() {
    let mut v = vec![1, 2, 3, 4, 5];
    let first = &v[0];
    v.push(6); // ERROR here because v is binding to immutable variable ~> v is frozen
}
```

# Hash Map
The type HashMap<K, V> stores a mapping of keys of type K to values of type V using a hashing function, which determines how it places these keys and values into memory

A HashMap is not in order, so if you print every key in a HashMap together it will probably print differently

```rust
fn main() {
    let mut scores = HashMap::new();
    scores.insert(String::from("Blue"), 10);

    let team_name = String::from("Blue");
    let score = scores.get(&team_name).copied().unwrap_or(0);

    for (key, value) in &scores {
        println!("{}: {}", key, value);
    }
    
    
}
```

```rust
fn main() {
    let mut scores = HashMap::new();
    scores.insert(String::from("Blue"), 10);
    scores.insert(String::from("Blue"), 11);            // Overwriting a Value
    scores.entry(String::from("Yellow")).or_insert(50); // Adding a Key and Value Only If a Key Isn’t Present

    let count = map.entry(String::from("Yellow1")).or_insert(0); // Updating a Value Based on the Old Value
    *count += 1;
}
```

Any type that implements the Eq and Hash traits can be a key in HashMap
- bool
- int, uint, and all variations thereof
- String and &str

f32 and f64 do not implement Hash because floating-point precision errors would make using them as hashmap keys horribly error-prone

All collection classes implement Eq and Hash if their contained type also respectively implements Eq and Hash. For example, Vec<T> will implement Hash if T implements Hash

You can easily implement `Eq and Hash` for a custom type with just one line: `#[derive(PartialEq, Eq, Hash)]`

If you want a HashMap that you can sort, you can use a `BTreeMap`

# HashSet
A HashSet's unique feature is that it is guaranteed to not have duplicate elements

Sets have 4 primary operations
- `union`: get all the unique elements in both sets.

- `difference`: get all the elements that are in the first set but not the second.

- `intersection`: get all the elements that are only in both sets.

- `symmetric_difference`: get all the elements that are in one set or the other, but not both.

```rust
fn main() {
    let hash_set_1: HashSet<i32> = vec![1, 2, 3].into_iter().collect();
    println!("{:?}", hash_set_1);

    let mut hash_set_2: HashSet<i32> = HashSet::new();
    hash_set_2.insert(1);
    hash_set_2.insert(3);
    hash_set_2.insert(4);

    let all: Vec<&i32> = hash_set_1.union(&hash_set_2).collect(); // ~> 1,2,3,4
    let diff: Vec<&i32> = hash_set_1.difference(&&hash_set_2).collect(); // ~> 2
    let inter: Vec<&i32> = hash_set_1.intersection(&hash_set_2).collect(); // ~> 1, 3
    let sy_diff: Vec<&i32> = hash_set_1.symmetric_difference(&hash_set_2).collect(); // ~> 2, 4
}
```

If you want a HashSet that you can sort, you can use a `BTreeSet`

# BinaryHeap
It keeps the largest item in the front

```rust
fn main() {
    let mut jobs = BinaryHeap::new();

    // Add jobs to do throughout the day
    jobs.push((100, "Write back to email from the CEO"));
    jobs.push((80, "Finish the report today"));
    jobs.push((5, "Watch some YouTube"));
    jobs.push((70, "Tell your team members thanks for always working hard"));
    jobs.push((30, "Plan who to hire next for the team"));

    while let Some(job) = jobs.pop() {
        println!("You need to: {}", job.1);
    }
    
    // Output: 100 ~> 80 ~> 70 ~> 30 ~> 5 ( decrease by key )
}
```

# VecDeque
A VecDeque is a Vec that is good at popping items both off the front and the back

VecDeque can be actived as a queue

```rust
fn main() {
    let mut my_vec = VecDeque::from(vec![0; 600000]);
    for i in 0..600000 {
        my_vec.pop_front(); // pop_front is like .pop but for the front
    }
}
```