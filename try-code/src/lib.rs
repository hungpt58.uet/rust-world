use std::collections::{BinaryHeap, HashMap, HashSet};

static LANGUAGE: &str = "Rust";

type X = u32;

struct Person {
    name: String,
    age: u8
}

impl Person {
    fn new(age: u8) -> Self {
        Person {
            age,
            name: String::from("Hello World")
        }
    }
}

enum Event {
    DeletePost,
    CreatePost(String)
}

impl Event {
    fn show(&self) {
        match self {
            Event::DeletePost => println!("DeletePost"),
            Event::CreatePost(str) => println!("{}", str)
        };
    }
}

fn show_character() {
    for c in 'A'..='Z' {
        println!("{}", c);
    }
}

fn add(x1: &u8, x2: &u8) -> u8 {
    x1 + x2
}

fn printer<T>(t: T) {
    println!("hihi");
}

fn format_str(str: &str) -> String {
    let mut output: String = String::new();

    for word in str.split_whitespace() {
        let mut ch: Vec<char> = word.chars().collect();
        ch[0] = ch[0].to_uppercase().nth(0).unwrap();

        let new_word = String::from_iter(ch);
        output.push_str(&new_word);
    }

    output
}

pub trait Behavior {
    fn show_msg(&self);
    fn me(&self) {
        println!("This is behavior");
    }
}

struct Bird;
struct Fish;

impl Bird {
    fn foo<'a, 'b>(&'a self, x: &'b i32) -> &'a i32 {
        todo!()
    }

    fn foo_2(&self, b: &i32) -> &i32 {
        todo!()
    }
}

impl Behavior for Bird {
    fn show_msg(&self) {
        println!("This is a bird with message");
    }
}

impl Behavior for Fish {
    fn show_msg(&self) {
        todo!()
    }
}

impl Default for Bird {
    fn default() -> Self { Bird }
}

pub fn notify<T: Behavior>(item: T) {
    item.show_msg();
}

pub fn test(flag: bool) -> Box<dyn Behavior> {
    if flag {
        Box::new(Bird)
    } else {
        Box::new(Fish)
    }
}

struct Adventurer<'a> {
    name: &'a str,
    hit_points: u32,
}

impl Adventurer<'_> {
    fn take_damage(&mut self) {
        self.hit_points -= 20;
        println!("{} has {} hit points left!", self.name, self.hit_points);
    }
}

pub fn old_main() {
    let mut jobs = BinaryHeap::new();

    // Add jobs to do throughout the day
    jobs.push((100, "Write back to email from the CEO"));
    jobs.push((80, "Finish the report today"));
    jobs.push((5, "Watch some YouTube"));
    jobs.push((70, "Tell your team members thanks for always working hard"));
    jobs.push((30, "Plan who to hire next for the team"));

    while let Some(job) = jobs.pop() {
        println!("You need to: {}", job.1);
    }

    //==============================================================================//
    Event::CreatePost("CreatePost".to_string()).show();
    println!("Hello, world! {:?}", LANGUAGE);
    //==============================================================================//

    println!("{}", format_str("Xin chào Việt Nam thân mến"));
    println!("{}", add(&1, &2));

    // Bird::me();
    // Bird::say();
    Bird.show_msg();
    Bird::default();

    let number = 15;
    let opt = Some(1);

    // Check number
    match number {
        k if k > 0 => println!("> 0"),
        m if m == 0 => println!("= 0"),
        _ => println!("< 0")
    }
    // Check option
    match opt {
        Some(v) => println!("{}", v),
        None => println!("None")
    }


    let f1 = |x: i8| x + 1;
    let result: i8 = vec![1,2,3].iter()
                                .map(|v| v + 1)
                                .filter(|&v| v > 2)
                                .sum();

    println!("{}", result);

    let p1 = Person {
        name: String::from("Name"),
        age: 1
    };
    // Copy with Struct
    // Set age = 2 & copy another values from p1
    // Same func `copy` with Scala case-class
    let p2 = Person {
        age: 2, ..p1
    };
    let k = 1;
    let m = &k;
    let i = &k + 1;

    println!("{} - {}", m, i);
}