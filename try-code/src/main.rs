use crate::smart_pointer::CpBox;

mod smart_pointer {
    use std::ops::Deref;

    pub struct MyBox<T>(T);

    impl<T> MyBox<T> {
        pub fn new(x: T) -> MyBox<T> {
            MyBox(x)
        }
    }

    impl<T> Deref for MyBox<T> {
        type Target = T;

        fn deref(&self) -> &Self::Target {
            &self.0
        }
    }

    pub struct NewBox {
        pub name: String,
        pub cp: CpBox,
    }

    pub struct CpBox {
        pub name: String,
    }

    impl Deref for NewBox {
        type Target = CpBox;

        fn deref(&self) -> &Self::Target { &self.cp }
    }

    pub fn handle(cp_box: &CpBox) {
        println!("Value = {}", cp_box.name)
    }

    pub fn main() {
        let str = "Hello World";
        let my_box = MyBox::new(str);
        let cp = *my_box; // want to use `*`, need `Deref` trait

        println!("{}", cp);

        let new_box = NewBox {
            name: String::from("Hello"),
            cp: CpBox { name: String::from("Hello") },
        };
        handle(&new_box);
    }
}

mod fearless_concurrency {
    use std::rc::Rc;
    use std::sync::{Arc, mpsc, Mutex};
    use std::thread;
    use std::time::Duration;

// multiple producer, single consumer

    pub fn run_single() {
        // create a new thread
        let all = thread::spawn(|| {
            for i in 1..10 {
                println!("hi number {} from the spawned thread!", i);
                thread::sleep(Duration::from_millis(1));
            }
        });

        let v = vec![1, 2, 3];
        let handle = thread::spawn(move || {
            println!("Here's a vector: {:?}", v);
        });

        handle.join().unwrap();
        all.join().unwrap();
    }

    pub fn run_channel() {
        let (tx, rx) = mpsc::channel();
        let tx2 = tx.clone();

        thread::spawn(move || {
            let vals = vec![
                String::from("hi"),
                String::from("from"),
                String::from("the"),
                String::from("thread"),
            ];

            for val in vals {
                tx.send(val).unwrap();
            }
        });

        thread::spawn(move || {
            let vals = vec![
                String::from("hello"),
                String::from("how"),
                String::from("are"),
                String::from("you"),
            ];

            for val in vals {
                tx2.send(val).unwrap();
            }
        });

        loop {
            // let _block_received = rx.recv();
            let non_block_received = rx.try_recv();

            if non_block_received.is_ok() {
                println!("Got: {}", non_block_received.unwrap());
            }
        }
    }

    pub fn run_mutex() {
        // use Arc to share data between threads
        let m = Arc::new(Mutex::new(1));
        let mut threads = vec![];

        for _ in 1..10 {
            let m = Arc::clone(&m);
            threads.push(
                thread::spawn(move || {
                    let mut v = m.lock().unwrap(); // lock variable
                    *v = *v + 2;
                })
            )
        }

        for th in threads {
            th.join().unwrap();
        }

        println!("{}", m.lock().unwrap())
    }
}

mod tokyo_async {
    use tokio::sync::mpsc;
    use tokio::sync::oneshot;

    #[tokio::main]
    pub async fn run() {
        let (tx, mut rx) = mpsc::channel(32);
        let tx2 = tx.clone();

        let x = tokio::spawn(async move {
            tx.send("sending from first handle").await.unwrap();
            1
        });

        let y = tokio::spawn(async move {
            tx2.send("sending from second handle").await.unwrap();
            2
        });

        let (a, b) = tokio::join!(x, y);
        println!("DONE: {}", (a.unwrap() + b.unwrap()));

        while let Some(message) = rx.recv().await {
            println!("GOT = {}", message);
        }

        let (tx1, rx1) = oneshot::channel();
        let (tx2, rx2) = oneshot::channel();

        tokio::spawn(async {
            let _ = tx1.send("one");
        });

        tokio::spawn(async {
            let _ = tx2.send("two");
        });

        tokio::select! {
            val = rx1 => {
                println!("rx1 completed first with {:?}", val);
            }
            val = rx2 => {
                println!("rx2 completed first with {:?}", val);
            }
        }
    }
}

fn main() {
    tokyo_async::run()
}
